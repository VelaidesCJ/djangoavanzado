# djangoAvanzado

Academic work

## Getting started

1. instalar las dependencias contenidas en el archivo requirements.txt

2. install and configure rabbitmq:
### sudo apt-get install rabbitmq-server
### sudo rabbitmqctl add_user <myuser> <mypassword>
### sudo rabbitmqctl add_vhost <myvhost>
### sudo rabbitmqctl set_permissions -p <myvhost> <myuser> ".*" ".*" ".*"

example:
### sudo apt-get install rabbitmq-server
### sudo rabbitmqctl add_user luis 123456
### sudo rabbitmqctl add_vhost djangoAdvance
### sudo rabbitmqctl set_permissions -p djangoAdvance luis ".*" ".*" ".*"


3. create a file called settings_local.py in the main project and add the user, password and name of the project configured with rabbitmq

example: 
CELERY_BROKER_URL = 'amqp://<USERNAME>:<PASSWORD>@localhost:5672/<VHOST>'
